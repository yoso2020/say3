export type PlayerColor = 'Blue' | 'Red' | 'Unknown';
export type PieceState = 'Unactivated' | 'Activated' | 'Marking' | 'Marked' | 'Deleting' | 'MovingOut' | 'MovingIn' | 'Deleted';
export type BattleState = 'NotStarted' | 'Preparing' | 'Started' | 'Finished';
export type EventType = BattleState | 'Starting' | 'AiMove' | 'AiMoving' | 'AiMarking' | 'AiDeleting';