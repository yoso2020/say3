import {Piece} from './piece'
import {PlayerColor} from './types'

export class Player {
  logs: any[] = []
  color: PlayerColor
  opponentColor: PlayerColor
  steps = 0
  doMarking = false
  doDeleting = false
  doMoving = false
  won = false
  name: string
  wonTime = 0

  constructor(color: PlayerColor) {
    this.color = color
    this.name = color === 'Red' ? '红色玩家' : '蓝色玩家'
    this.opponentColor = color === 'Red' ? 'Blue' : 'Red'
  }

  move(piece: Piece, action: string) {
    this.logs.push({action, piece})
    this.steps++
  }

  reset() {
    this.doMoving = false
    this.doDeleting = false
    this.doMarking = false
    this.won = false
    this.steps = 0
    this.logs = []
  }

  setWon() {
    this.wonTime ++
    this.won = true
  }
}