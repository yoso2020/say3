import {Piece} from './models/piece'

export class Utils {
  static getRandomInt(max, min = 0): number {
    return Math.round(Math.random() * (max - min)) + min
  }

  static flat(arr: any[][]): any[] {
    return arr.reduce((acc, curVal) => {
      return acc.concat(Array.isArray(curVal) ? curVal : [curVal])
    }, [])
  }

  static clone<T>(t: T): T {
    return JSON.parse(JSON.stringify(t))
  }
}